
var count= 0;

$(document).ready(function () {
    $("#btn").click(function () {
        var ourRequest = new XMLHttpRequest();
        ourRequest.open('GET','https://learnwebcode.github.io/json-example/animals-1.json');
        ourRequest.onload=function () {
            var ourData = JSON.parse(ourRequest.responseText);

            var myHTML = ourData[count].name+"<br>";
                myHTML += ourData[count].species+"<br>";
                myHTML += ourData[count].foods.likes+"<br>";
                myHTML += ourData[count].foods.dislikes+"<br>";

            $("#animal-info").html(myHTML);
           $("#animal-info").css("background","GREEN");

            count++;

        };
        ourRequest.send();
    })
})